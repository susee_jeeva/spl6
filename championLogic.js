const ChampionAlgorithm = require('./championAlgorithm.js')

class ChampionLogic {
  constructor(data) {
    this.boardInfo = data.boardInfo;
    this.boardSize = data.boardSize[0];
    this.myId = data.yourID;
    this.opponentId = data.yourID == 1 ? 2 : 1;
    this.grantedSP = data.grantedSP;
    this.usedSP = data.usedSP;
    this.yourSP = data.yourSP;
  }

  calculateSum(array) {
    function myFunc(total, num) {
      return total + num;
    }
    return array.reduce(myFunc);
  }

  cloneBoard(boardInfo) {
    return JSON.parse(JSON.stringify(boardInfo));
  }

  getBestMove(objArray, playerId) {
    objArray = objArray.sort((a, b) => {
      return b[`weightageArray${playerId}`][0] - a[`weightageArray${playerId}`][0];
    })
    return objArray[0];
  }

  getBombMove() {
    let bombMoveData = this.getEmptyCellObjects(this.boardInfo, this.boardSize);
    for (let i = 0; i < bombMoveData.length; i++) {
      let myBoard1 = this.cloneBoard(this.boardInfo);
      let obj = bombMoveData[i];
      let championAlgorithm = new ChampionAlgorithm(myBoard1, this.boardSize, this.myId, obj, this.opponentId);
      championAlgorithm.plantBomb();

      //opponent move 1
      this.oppMove1Data = this.getEmptyCellObjects(myBoard1, this.boardSize);
      for (let i = 0; i < this.oppMove1Data.length; i++) {
        let oppBoard1 = this.cloneBoard(myBoard1);
        let obj = this.oppMove1Data[i];
        let championAlgorithm = new ChampionAlgorithm(oppBoard1, this.boardSize, this.opponentId, obj, this.myId);
        championAlgorithm.evaluatePoint();
      }
      let opp1BestMove = this.getBestMove(this.oppMove1Data, this.opponentId);
      bombMoveData[i][`weightageArray${this.opponentId}`].push(opp1BestMove[`weightageArray${this.opponentId}`][0]);
      bombMoveData[i][`netWeightage${this.myId}`] = this.calculateSum(bombMoveData[i][`weightageArray${this.myId}`]);
      bombMoveData[i][`netWeightage${this.opponentId}`] = this.calculateSum(bombMoveData[i][`weightageArray${this.opponentId}`]);
    }
    let sortedMove = this.getSortedMoveByTwoWeightage(bombMoveData, this.myId, this.opponentId);
    let finalMove = sortedMove[0];
    return finalMove;
  }

  getComputedMove() {
    this.moveData = this.getEmptyCellObjects(this.boardInfo, this.boardSize);
    for (let i = 0; i < this.moveData.length; i++) {
      let myBoard1 = this.cloneBoard(this.boardInfo);
      let obj = this.moveData[i];
      let championAlgorithm = new ChampionAlgorithm(myBoard1, this.boardSize, this.myId, obj, this.opponentId);
      championAlgorithm.evaluatePoint();

      //opponent move 1
      this.oppMove1Data = this.getEmptyCellObjects(myBoard1, this.boardSize);
      for (let i = 0; i < this.oppMove1Data.length; i++) {
        let oppBoard1 = this.cloneBoard(myBoard1);
        let obj = this.oppMove1Data[i];
        let championAlgorithm = new ChampionAlgorithm(oppBoard1, this.boardSize, this.opponentId, obj, this.myId);
        championAlgorithm.evaluatePoint();
      }
      let opp1BestMove = this.getBestMove(this.oppMove1Data, this.opponentId);

      // //my move 2
      // this.myMove2Data = this.getEmptyCellObjects(opp1BestMove.boardInfo, this.boardSize);
      // for (let i = 0; i < this.myMove2Data.length; i++) {
      //   let myBoard2 = this.cloneBoard(opp1BestMove.boardInfo);
      //   let obj = this.myMove2Data[i];
      //   let championAlgorithm = new ChampionAlgorithm(myBoard2, this.boardSize, this.myId, obj, this.opponentId);
      //   championAlgorithm.evaluatePoint();
      // }
      // let my2BestMove = this.getBestMove(this.myMove2Data, this.myId);

      // //opponent move 2
      // this.oppMove2Data = this.getEmptyCellObjects(my2BestMove.boardInfo, this.boardSize);
      // for (let i = 0; i < this.oppMove2Data.length; i++) {
      //   let oppBoard2 = this.cloneBoard(my2BestMove.boardInfo);
      //   let obj = this.oppMove2Data[i];
      //   let championAlgorithm = new ChampionAlgorithm(oppBoard2, this.boardSize, this.opponentId, obj, this.myId);
      //   championAlgorithm.evaluatePoint();
      // }
      // let opp2BestMove = this.getBestMove(this.oppMove2Data, this.opponentId);

      this.moveData[i].opp1Data = this.oppMove1Data;
      // this.moveData[i].my2Data = this.myMove2Data;
      // this.moveData[i].opp2Data = this.oppMove2Data;
      this.moveData[i].opp1BestMove = opp1BestMove;
      // this.moveData[i].my2BestMove = my2BestMove;
      // this.moveData[i].opp2BestMove = opp2BestMove;

      this.moveData[i][`weightageArray${this.opponentId}`].push(opp1BestMove[`weightageArray${this.opponentId}`][0]);
      // this.moveData[i][`weightageArray${this.myId}`].push(my2BestMove[`weightageArray${this.myId}`][0]);
      // this.moveData[i][`weightageArray${this.opponentId}`].push(opp2BestMove[`weightageArray${this.opponentId}`][0]);
      this.moveData[i][`netWeightage${this.myId}`] = this.calculateSum(this.moveData[i][`weightageArray${this.myId}`]);
      this.moveData[i][`netWeightage${this.opponentId}`] = this.calculateSum(this.moveData[i][`weightageArray${this.opponentId}`]);
    }
    
    let sortedMove = this.getSortedMoveByTwoWeightage(this.moveData, this.myId, this.opponentId);
    let finalMove = sortedMove[0];
    
    if(this.isMoveHopeless(finalMove) && this.usedSP < this.grantedSP && this.yourSP.filter((val) => {
      return val == "S"
    }).length) {
      let superChargeMove = this.getSuperChargeMove();
      if(!this.isMoveHopeless(superChargeMove)) {
        return {
          computedMove: [superChargeMove.i, superChargeMove.j],
          particleType: 'S'
        };
      }
    }

    if(this.isMoveHopeless(finalMove) && this.usedSP < this.grantedSP && this.yourSP.filter((val) => {
      return val == "B"
    }).length) {
      let bombMove = this.getBombMove();
      return {
        computedMove: [bombMove.i, bombMove.j],
        particleType: 'B'
      };
    }
    return {
      computedMove: [finalMove.i, finalMove.j],
      particleType: 'C'
    };
  }

  getEmptyCellObjects(boardInfo, boardSize) {
    let result = [];
    for (let i = 0; i < boardSize; i++) {
      for (let j = 0; j < boardSize; j++) {
        if (boardInfo[i][j] == 0) {
          result.push({
            i: i,
            j: j,
            weightageArray1: [],
            weightageArray2: [],
            netWeightage1: 0,
            netWeightage2: 0
          });
        }
      }
    }
    return result;
  }
  
  getSortedMoveByTwoWeightage(objArray, playerId, opponentId) {
    let directWinningPoints = objArray.filter((obj) => {
      return obj[`weightageArray${playerId}`][0] >= 10000;
    });
    if(directWinningPoints.length > 0) {
      directWinningPoints.sort((a, b) => {
        return (a[`netWeightage${opponentId}`]) - (b[`netWeightage${opponentId}`]);
      });
      return directWinningPoints;
    }
    objArray = objArray.sort((a, b) => {
      return (b[`netWeightage${playerId}`] - b[`netWeightage${opponentId}`]) - (a[`netWeightage${playerId}`] - a[`netWeightage${opponentId}`]);
    });
    return objArray;
  }

  getSuperChargeMove() {
    let superChargeMoveData = this.getEmptyCellObjects(this.boardInfo, this.boardSize);
    for (let i = 0; i < superChargeMoveData.length; i++) {
      let myBoard1 = this.cloneBoard(this.boardInfo);
      let obj = superChargeMoveData[i];
      let championAlgorithm = new ChampionAlgorithm(myBoard1, this.boardSize, this.myId, obj, this.opponentId);
      championAlgorithm.plantSuperCharge();

      //opponent move 1
      this.oppMove1Data = this.getEmptyCellObjects(myBoard1, this.boardSize);
      for (let i = 0; i < this.oppMove1Data.length; i++) {
        let oppBoard1 = this.cloneBoard(myBoard1);
        let obj = this.oppMove1Data[i];
        let championAlgorithm = new ChampionAlgorithm(oppBoard1, this.boardSize, this.opponentId, obj, this.myId);
        championAlgorithm.evaluatePoint();
      }
      let opp1BestMove = this.getBestMove(this.oppMove1Data, this.opponentId);
      superChargeMoveData[i][`weightageArray${this.opponentId}`].push(opp1BestMove[`weightageArray${this.opponentId}`][0]);
      superChargeMoveData[i][`netWeightage${this.myId}`] = this.calculateSum(superChargeMoveData[i][`weightageArray${this.myId}`]);
      superChargeMoveData[i][`netWeightage${this.opponentId}`] = this.calculateSum(superChargeMoveData[i][`weightageArray${this.opponentId}`]);
    }
    let sortedMove = this.getSortedMoveByTwoWeightage(superChargeMoveData, this.myId, this.opponentId);
    let finalMove = sortedMove[0];
    return finalMove;
  }

  isMoveHopeless(obj) {
    return obj[`weightageArray${this.opponentId}`][0] >= 10000 && obj[`weightageArray${this.myId}`][0] < 10000;
  }
}

module.exports = ChampionLogic;