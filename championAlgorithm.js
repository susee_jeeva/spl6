class ChampionAlgorithm {

  constructor(boardInfo, boardSize, myId, obj, opponentId) {
    this.boardInfo = boardInfo;
    this.boardSize = boardSize;
    this.myId = myId;
    this.opponentId = opponentId;
    this.obj = obj;
  }

  calculateWeightage() {
    let weightage = 0;
    if (this.hasWinningChance()) {
      weightage = 10000;
    }

    let noOf3Pairs = this.getNumberOf3Pairs();
    weightage += (300 * noOf3Pairs);

    let chargeCount = this.getChargeCount(this.myId);
    weightage += (10 * chargeCount);

    let surroundingEmptyCells = this.getSurroundingEmptyCell(this.obj);
    let superSurroundingEmptyCells = [];
    for(let i=0; i<surroundingEmptyCells.length;i++) {
      superSurroundingEmptyCells = superSurroundingEmptyCells.concat(this.getSurroundingEmptyCell(surroundingEmptyCells[i]));
    }
    weightage += surroundingEmptyCells.length + superSurroundingEmptyCells.length;

    this.obj[`weightageArray${this.myId}`].push(weightage);
  }

  doAttraction() {
    this.evaluateAttractionEdges();
    this.evaluateAttractionCenters();
  }

  doBombSimulation() {
    let surroundingEmptyCells = this.getSurroundingCell(this.obj);
    for(let i=0; i<surroundingEmptyCells.length; i++) {
      if(this.boardInfo[surroundingEmptyCells[i].i][surroundingEmptyCells[i].j] == 1 || this.boardInfo[surroundingEmptyCells[i].i][surroundingEmptyCells[i].j] == 2) {
        this.boardInfo[surroundingEmptyCells[i].i][surroundingEmptyCells[i].j] = 0;
      }
    }
    this.calculateWeightage();
  }

  doRepulsion() {
    this.evaluateRepulsiveEdges();
    this.evaluateRepulsiveCenters();
  }

  doSimulation() {
    this.doAttraction();
    this.doRepulsion();
    this.calculateWeightage();
  }

  doSuperChargeSimulation() {
    let surroundingEmptyCells = this.getSurroundingCell(this.obj);
    for(let i=0; i<surroundingEmptyCells.length; i++) {
      if(this.boardInfo[surroundingEmptyCells[i].i][surroundingEmptyCells[i].j] == this.opponentId) {
        this.boardInfo[surroundingEmptyCells[i].i][surroundingEmptyCells[i].j] = 0;
      }
    }
    this.calculateWeightage();
  }

  evaluateAttractionCenters() {
    let centers = this.getCenters(this.obj).filter((obj) => {
      return this.isEmptyOrBlackHole(this.boardInfo[obj.i][obj.j]);
    });
    for (let i = 0; i < centers.length; i++) {
      let actualElement = centers[i];
      let superCenter = this.getCenters(actualElement).filter((obj) => {
        return obj.name == actualElement.name && this.boardInfo[obj.i][obj.j] == this.opponentId;
      });
      if (superCenter.length == 1) {
        this.boardInfo[superCenter[0].i][superCenter[0].j] = 0;
        if (this.isNotBlackHole(this.boardInfo[actualElement.i][actualElement.j])) {
          this.boardInfo[actualElement.i][actualElement.j] = this.opponentId;
        }
      } else {
        if (actualElement.name == "top") {
          let superTopLeft = this.getEdges(actualElement).filter((obj) => {
            return obj.name == 'topLeft' && this.boardInfo[obj.i][obj.j] == this.opponentId;
          });
          if (superTopLeft.length == 1) {
            this.boardInfo[superTopLeft[0].i][superTopLeft[0].j] = 0;
            this.boardInfo[actualElement.i][actualElement.j] = this.opponentId;
          } else {
            let superTopRight = this.getEdges(actualElement).filter((obj) => {
              return obj.name == 'topRight' && this.boardInfo[obj.i][obj.j] == this.opponentId;
            });
            if (superTopRight.length == 1) {
              this.boardInfo[superTopRight[0].i][superTopRight[0].j] = 0;
              if (this.isNotBlackHole(this.boardInfo[actualElement.i][actualElement.j])) {
                this.boardInfo[actualElement.i][actualElement.j] = this.opponentId;
              }
            }
          }
        } else if (actualElement.name == "bottom") {
          let superBottomLeft = this.getEdges(actualElement).filter((obj) => {
            return obj.name == 'bottomLeft' && this.boardInfo[obj.i][obj.j] == this.opponentId;
          });
          if (superBottomLeft.length == 1) {
            this.boardInfo[superBottomLeft[0].i][superBottomLeft[0].j] = 0;
            if (this.isNotBlackHole(this.boardInfo[actualElement.i][actualElement.j])) {
              this.boardInfo[actualElement.i][actualElement.j] = this.opponentId;
            }
          } else {
            let superBottomRight = this.getEdges(actualElement).filter((obj) => {
              return obj.name == 'bottomRight' && this.boardInfo[obj.i][obj.j] == this.opponentId;
            });
            if (superBottomRight.length == 1) {
              this.boardInfo[superBottomRight[0].i][superBottomRight[0].j] = 0;
              this.boardInfo[actualElement.i][actualElement.j] = this.opponentId;
            }
          }
        } else if (actualElement.name == "left") {
          let superTopLeft = this.getEdges(actualElement).filter((obj) => {
            return obj.name == 'topLeft' && this.boardInfo[obj.i][obj.j] == this.opponentId;
          });
          if (superTopLeft.length == 1) {
            this.boardInfo[superTopLeft[0].i][superTopLeft[0].j] = 0;
            if (this.isNotBlackHole(this.boardInfo[actualElement.i][actualElement.j])) {
              this.boardInfo[actualElement.i][actualElement.j] = this.opponentId;
            }
          } else {
            let superBottomLeft = this.getEdges(actualElement).filter((obj) => {
              return obj.name == 'bottomLeft' && this.boardInfo[obj.i][obj.j] == this.opponentId;
            });
            if (superBottomLeft.length == 1) {
              this.boardInfo[superBottomLeft[0].i][superBottomLeft[0].j] = 0;
              if (this.isNotBlackHole(this.boardInfo[actualElement.i][actualElement.j])) {
                this.boardInfo[actualElement.i][actualElement.j] = this.opponentId;
              }
            }
          }
        } else if (actualElement.name == "right") {
          let superTopRight = this.getEdges(actualElement).filter((obj) => {
            return obj.name == 'topRight' && this.boardInfo[obj.i][obj.j] == this.opponentId;
          });
          if (superTopRight.length == 1) {
            this.boardInfo[superTopRight[0].i][superTopRight[0].j] = 0;
            if (this.isNotBlackHole(this.boardInfo[actualElement.i][actualElement.j])) {
              this.boardInfo[actualElement.i][actualElement.j] = this.opponentId;
            }
          } else {
            let superBottomRight = this.getEdges(actualElement).filter((obj) => {
              return obj.name == 'bottomRight' && this.boardInfo[obj.i][obj.j] == this.opponentId;
            });
            if (superBottomRight.length == 1) {
              this.boardInfo[superBottomRight[0].i][superBottomRight[0].j] = 0;
              if (this.isNotBlackHole(this.boardInfo[actualElement.i][actualElement.j])) {
                this.boardInfo[actualElement.i][actualElement.j] = this.opponentId;
              }
            }
          }
        }
      }
    }
  }

  evaluateAttractionEdges() {
    let edges = this.getEdges(this.obj).filter((obj) => {
      return this.isEmptyOrBlackHole(this.boardInfo[obj.i][obj.j]);
    });
    for (let i = 0; i < edges.length; i++) {
      let actualElement = edges[i];
      let superEdge = this.getEdges(actualElement).filter((obj) => {
        return obj.name == actualElement.name && this.boardInfo[obj.i][obj.j] == this.opponentId;
      });
      if (superEdge.length == 1) {
        this.boardInfo[superEdge[0].i][superEdge[0].j] = 0;
        if (this.isNotBlackHole(this.boardInfo[actualElement.i][actualElement.j])) {
          this.boardInfo[actualElement.i][actualElement.j] = this.opponentId;
        }
      }
    }
  }

  evaluateRepulsiveCenters() {
    let centers = this.getCenters(this.obj).filter((obj) => {
      return this.boardInfo[obj.i][obj.j] == this.myId;
    });
    for (let i = 0; i < centers.length; i++) {
      let actualElement = centers[i];
      let superCenter = this.getCenters(actualElement, true).filter((obj) => {
        return obj.name == actualElement.name && (this.boardInfo[obj.i] == undefined || this.boardInfo[obj.i][obj.j] == undefined || this.isEmptyOrBlackHole(this.boardInfo[obj.i][obj.j]));
      });
      if (superCenter.length == 1) {
        if (superCenter[0].i >= 0 && superCenter[0].i < this.boardSize && superCenter[0].j >= 0 && superCenter[0].j < this.boardSize) {
          if (this.isNotBlackHole(this.boardInfo[superCenter[0].i][superCenter[0].j])) {
            this.boardInfo[superCenter[0].i][superCenter[0].j] = this.myId;
          }
        }
        this.boardInfo[actualElement.i][actualElement.j] = 0;
      }
    }
  }

  evaluateRepulsiveEdges() {
    let edges = this.getEdges(this.obj).filter((obj) => {
      return this.boardInfo[obj.i][obj.j] == this.myId;
    });
    for (let i = 0; i < edges.length; i++) {
      let actualElement = edges[i];
      let superEdge = this.getEdges(actualElement, true).filter((obj) => {
        return obj.name == actualElement.name && (this.boardInfo[obj.i] == undefined || this.boardInfo[obj.i][obj.j] == undefined || this.isEmptyOrBlackHole(this.boardInfo[obj.i][obj.j]));
      });
      if (superEdge.length == 1) {
        if (superEdge[0].i >= 0 && superEdge[0].i < this.boardSize && superEdge[0].j >= 0 && superEdge[0].j < this.boardSize) {
          if (this.isNotBlackHole(this.boardInfo[superEdge[0].i][superEdge[0].j])) {
            this.boardInfo[superEdge[0].i][superEdge[0].j] = this.myId;
          }
        }
        this.boardInfo[actualElement.i][actualElement.j] = 0;
      }
    }
  }

  evaluatePoint() {
    this.plotObjPoint();
    this.doSimulation();
    this.obj.boardInfo = this.boardInfo;
    // console.log((this.obj.i * 10) + (this.obj.j + 1));
    // console.log(this.obj[`weightageArray${this.myId}`]);
    // console.log(this.boardInfo);
  }

  getCenters(localObj, canIncludeElementsOutOfBorder) {
    let result = [];
    result.push({
      i: localObj.i,
      j: localObj.j - 1,
      name: 'left'
    });
    result.push({
      i: localObj.i + 1,
      j: localObj.j,
      name: 'bottom'
    });
    result.push({
      i: localObj.i,
      j: localObj.j + 1,
      name: 'right'
    });
    result.push({
      i: localObj.i - 1,
      j: localObj.j,
      name: 'top'
    });

    if (canIncludeElementsOutOfBorder) {
      return result;
    }

    return result.filter((obj) => {
      return obj.i >= 0 && obj.j >= 0 && obj.i < this.boardSize && obj.j < this.boardSize;
    })
  }

  getChargeCount(playerId) {
    let count = 0;
    for (let i = 0; i < this.boardSize; i++) {
      for (let j = 0; j < this.boardSize; j++) {
        if (this.boardInfo[i][j] == playerId) {
          count++;
        }
      }
    }
    return count;
  }

  getEdges(localObj, canIncludeElementsOutOfBorder) {
    let result = [];
    result.push({
      i: localObj.i - 1,
      j: localObj.j - 1,
      name: 'topLeft'
    });
    result.push({
      i: localObj.i + 1,
      j: localObj.j - 1,
      name: 'bottomLeft'
    });
    result.push({
      i: localObj.i - 1,
      j: localObj.j + 1,
      name: 'topRight'
    });
    result.push({
      i: localObj.i + 1,
      j: localObj.j + 1,
      name: 'bottomRight'
    });

    if (canIncludeElementsOutOfBorder) {
      return result;
    }

    return result.filter((obj) => {
      return obj.i >= 0 && obj.j >= 0 && obj.i < this.boardSize && obj.j < this.boardSize;
    });
  }

  getNumberOf3Pairs() {
    let count = 0;
    for (let i = 0; i < this.boardSize; i++) {
      for (let j = 0; j < this.boardSize; j++) {
        if (this.boardInfo[i][j] == this.myId) {

          //calculate right
          if (this.boardInfo[i][j + 1] != undefined && this.boardInfo[i][j + 2] != undefined && this.boardInfo[i][j + 3] != undefined) {
            if (this.boardInfo[i][j + 1] == this.myId && this.boardInfo[i][j + 2] == this.myId && this.boardInfo[i][j + 3] == 0) {
              count++;
            }
          }
          //calculate left
          if (this.boardInfo[i][j - 1] != undefined && this.boardInfo[i][j - 2] != undefined && this.boardInfo[i][j - 3] != undefined) {
            if (this.boardInfo[i][j - 1] == this.myId && this.boardInfo[i][j - 2] == this.myId && this.boardInfo[i][j - 3] == 0) {
              count++;
            }
          }
          //calculate bottom
          if (this.boardInfo[i + 1] != undefined && this.boardInfo[i + 1][j] != undefined && this.boardInfo[i + 2] != undefined && this.boardInfo[i + 2][j] != undefined && this.boardInfo[i + 3] != undefined && this.boardInfo[i + 3][j] != undefined) {
            if (this.boardInfo[i + 1][j] == this.myId && this.boardInfo[i + 2][j] == this.myId && this.boardInfo[i + 3][j] == 0) {
              count++;
            }
          }
          //calculate top
          if (this.boardInfo[i - 1] != undefined && this.boardInfo[i - 1][j] != undefined && this.boardInfo[i - 2] != undefined && this.boardInfo[i - 2][j] != undefined && this.boardInfo[i - 3] != undefined && this.boardInfo[i - 3][j] != undefined) {
            if (this.boardInfo[i - 1][j] == this.myId && this.boardInfo[i - 2][j] == this.myId && this.boardInfo[i - 3][j] == 0) {
              count++;
            }
          }
          //calculate top right
          if (this.boardInfo[i - 1] != undefined && this.boardInfo[i - 1][j + 1] != undefined && this.boardInfo[i - 2] != undefined && this.boardInfo[i - 2][j + 2] != undefined && this.boardInfo[i - 3] != undefined && this.boardInfo[i - 3][j + 3] != undefined) {
            if (this.boardInfo[i - 1][j + 1] == this.myId && this.boardInfo[i - 2][j + 2] == this.myId && this.boardInfo[i - 3][j + 3] == 0) {
              count++;
            }
          }
          //calculate top left
          if (this.boardInfo[i - 1] != undefined && this.boardInfo[i - 1][j - 1] != undefined && this.boardInfo[i - 2] != undefined && this.boardInfo[i - 2][j - 2] != undefined && this.boardInfo[i - 3] != undefined && this.boardInfo[i - 3][j - 3] != undefined) {
            if (this.boardInfo[i - 1][j - 1] == this.myId && this.boardInfo[i - 2][j - 2] == this.myId && this.boardInfo[i - 3][j - 3] == 0) {
              count++;
            }
          }
          //calculate bottom right
          if (this.boardInfo[i + 1] != undefined && this.boardInfo[i + 1][j + 1] != undefined && this.boardInfo[i + 2] != undefined && this.boardInfo[i + 2][j + 2] != undefined && this.boardInfo[i + 3] != undefined && this.boardInfo[i + 3][j + 3] != undefined) {
            if (this.boardInfo[i + 1][j + 1] == this.myId && this.boardInfo[i + 2][j + 2] == this.myId && this.boardInfo[i + 3][j + 3] == 0) {
              count++;
            }
          }
          //calculate bottom left
          if (this.boardInfo[i + 1] != undefined && this.boardInfo[i + 1][j - 1] != undefined && this.boardInfo[i + 2] != undefined && this.boardInfo[i + 2][j - 2] != undefined && this.boardInfo[i + 3] != undefined && this.boardInfo[i + 3][j - 3] != undefined) {
            if (this.boardInfo[i + 1][j - 1] == this.myId && this.boardInfo[i + 2][j - 2] == this.myId && this.boardInfo[i + 3][j - 3] == 0) {
              count++;
            }
          }

        }
      }
    }
    return count;
  }

  getSurroundingCell(localObj) {
    let result = [];
    result.push({
      i: localObj.i - 1,
      j: localObj.j - 1,
      name: 'topLeft'
    });
    result.push({
      i: localObj.i + 1,
      j: localObj.j - 1,
      name: 'bottomLeft'
    });
    result.push({
      i: localObj.i - 1,
      j: localObj.j + 1,
      name: 'topRight'
    });
    result.push({
      i: localObj.i + 1,
      j: localObj.j + 1,
      name: 'bottomRight'
    });
    result.push({
      i: localObj.i,
      j: localObj.j - 1,
      name: 'left'
    });
    result.push({
      i: localObj.i + 1,
      j: localObj.j,
      name: 'bottom'
    });
    result.push({
      i: localObj.i,
      j: localObj.j + 1,
      name: 'right'
    });
    result.push({
      i: localObj.i - 1,
      j: localObj.j,
      name: 'top'
    });

    return result.filter((obj) => {
      return obj.i >= 0 && obj.j >= 0 && obj.i < this.boardSize && obj.j < this.boardSize;
    });
  }

  getSurroundingEmptyCell(localObj) {
    let result = [];
    result.push({
      i: localObj.i - 1,
      j: localObj.j - 1,
      name: 'topLeft'
    });
    result.push({
      i: localObj.i + 1,
      j: localObj.j - 1,
      name: 'bottomLeft'
    });
    result.push({
      i: localObj.i - 1,
      j: localObj.j + 1,
      name: 'topRight'
    });
    result.push({
      i: localObj.i + 1,
      j: localObj.j + 1,
      name: 'bottomRight'
    });
    result.push({
      i: localObj.i,
      j: localObj.j - 1,
      name: 'left'
    });
    result.push({
      i: localObj.i + 1,
      j: localObj.j,
      name: 'bottom'
    });
    result.push({
      i: localObj.i,
      j: localObj.j + 1,
      name: 'right'
    });
    result.push({
      i: localObj.i - 1,
      j: localObj.j,
      name: 'top'
    });

    return result.filter((obj) => {
      return obj.i >= 0 && obj.j >= 0 && obj.i < this.boardSize && obj.j < this.boardSize && this.boardInfo[obj.i][obj.j] == 0;
    });
  }

  hasWinningChance() {
    for (let i = 0; i < this.boardSize; i++) {
      for (let j = 0; j < this.boardSize; j++) {
        if (this.boardInfo[i][j] == this.myId) {

          //calculate right
          if (this.boardInfo[i][j + 1] != undefined && this.boardInfo[i][j + 2] != undefined && this.boardInfo[i][j + 3] != undefined) {
            if (this.boardInfo[i][j + 1] == this.myId && this.boardInfo[i][j + 2] == this.myId && this.boardInfo[i][j + 3] == this.myId) {
              return true;
            }
          }
          //calculate left
          if (this.boardInfo[i][j - 1] != undefined && this.boardInfo[i][j - 2] != undefined && this.boardInfo[i][j - 3] != undefined) {
            if (this.boardInfo[i][j - 1] == this.myId && this.boardInfo[i][j - 2] == this.myId && this.boardInfo[i][j - 3] == this.myId) {
              return true;
            }
          }
          //calculate bottom
          if (this.boardInfo[i + 1] != undefined && this.boardInfo[i + 1][j] != undefined && this.boardInfo[i + 2] != undefined && this.boardInfo[i + 2][j] != undefined && this.boardInfo[i + 3] != undefined && this.boardInfo[i + 3][j] != undefined) {
            if (this.boardInfo[i + 1][j] == this.myId && this.boardInfo[i + 2][j] == this.myId && this.boardInfo[i + 3][j] == this.myId) {
              return true;
            }
          }
          //calculate top
          if (this.boardInfo[i - 1] != undefined && this.boardInfo[i - 1][j] != undefined && this.boardInfo[i - 2] != undefined && this.boardInfo[i - 2][j] != undefined && this.boardInfo[i - 3] != undefined && this.boardInfo[i - 3][j] != undefined) {
            if (this.boardInfo[i - 1][j] == this.myId && this.boardInfo[i - 2][j] == this.myId && this.boardInfo[i - 3][j] == this.myId) {
              return true;
            }
          }
          //calculate top right
          if (this.boardInfo[i - 1] != undefined && this.boardInfo[i - 1][j + 1] != undefined && this.boardInfo[i - 2] != undefined && this.boardInfo[i - 2][j + 2] != undefined && this.boardInfo[i - 3] != undefined && this.boardInfo[i - 3][j + 3] != undefined) {
            if (this.boardInfo[i - 1][j + 1] == this.myId && this.boardInfo[i - 2][j + 2] == this.myId && this.boardInfo[i - 3][j + 3] == this.myId) {
              return true;
            }
          }
          //calculate top left
          if (this.boardInfo[i - 1] != undefined && this.boardInfo[i - 1][j - 1] != undefined && this.boardInfo[i - 2] != undefined && this.boardInfo[i - 2][j - 2] != undefined && this.boardInfo[i - 3] != undefined && this.boardInfo[i - 3][j - 3] != undefined) {
            if (this.boardInfo[i - 1][j - 1] == this.myId && this.boardInfo[i - 2][j - 2] == this.myId && this.boardInfo[i - 3][j - 3] == this.myId) {
              return true;
            }
          }
          //calculate bottom right
          if (this.boardInfo[i + 1] != undefined && this.boardInfo[i + 1][j + 1] != undefined && this.boardInfo[i + 2] != undefined && this.boardInfo[i + 2][j + 2] != undefined && this.boardInfo[i + 3] != undefined && this.boardInfo[i + 3][j + 3] != undefined) {
            if (this.boardInfo[i + 1][j + 1] == this.myId && this.boardInfo[i + 2][j + 2] == this.myId && this.boardInfo[i + 3][j + 3] == this.myId) {
              return true;
            }
          }
          //calculate bottom left
          if (this.boardInfo[i + 1] != undefined && this.boardInfo[i + 1][j - 1] != undefined && this.boardInfo[i + 2] != undefined && this.boardInfo[i + 2][j - 2] != undefined && this.boardInfo[i + 3] != undefined && this.boardInfo[i + 3][j - 3] != undefined) {
            if (this.boardInfo[i + 1][j - 1] == this.myId && this.boardInfo[i + 2][j - 2] == this.myId && this.boardInfo[i + 3][j - 3] == this.myId) {
              return true;
            }
          }

        }
      }
    }
    return false;
  }

  isEmptyOrBlackHole(value) {
    return value == 0 || value == -1;
  }

  isNotBlackHole(value) {
    return value != -1;
  }

  plantBomb() {
    this.doBombSimulation();
    this.obj.boardInfo = this.boardInfo;
  }

  plotObjPoint() {
    this.boardInfo[this.obj.i][this.obj.j] = this.myId;
  }

  plantSuperCharge() {
    this.doSuperChargeSimulation();
    this.obj.boardInfo = this.boardInfo;
  }
}


module.exports = ChampionAlgorithm;